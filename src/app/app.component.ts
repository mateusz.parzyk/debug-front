import { Component } from '@angular/core';

const foo = { name: 'tom', age: 30, nervous: false };
const bar = { name: 'dick', age: 40, nervous: false };
const baz = { name: 'harry', age: 50, nervous: true };
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  hoverClass = null;

  normalLog() {
    console.log(foo);
    console.log(bar);
    console.log(baz);
    console.info('Something happened…'); // same as console log
    console.warn('Something strange happened…'); // same as console log but outputs a warning
    console.error('Something horrible happened…'); // same as console log but outputs an error
    console.log('%c My Friends', 'background: #FF00FF; color: orange; font-weight: bold;');
    console.log('%c Rainbowww!', 'font-weight: bold; font-size: 50px;color: red; text-shadow: 3px 3px 0 rgb(217,31,38) ,' +
    ' 6px 6px 0 rgb(226,91,14) , 9px 9px 0 rgb(245,221,8) , 12px 12px 0 rgb(5,148,68) , 15px 15px 0 rgb(2,135,206) ,' +
    ' 18px 18px 0 rgb(4,77,145) , 21px 21px 0 rgb(42,21,113)');
  }

  computedPropertyLog() {
    console.log({ foo, bar, baz });
  }

  referenceLog() {
    let a = {
      foo: foo,
      bar: bar,
      baz: baz
    };
    console.log(a);
    console.log(a.foo);
    console.log(Object.assign({}, a));
    a.foo = { name: 'new', age: 1000, nervous: true };
  }

  consoleTable() {
    console.table([foo, bar, baz]);
  }

  consoleTimer() {
    console.time('looper');

    let i = 0;
    while (i < 1000000) { i++ };

    console.timeEnd('looper');
  }

  deleteMe() {
    console.trace('bye bye database');
  }

  traceLog() {
    this.deleteMe();
    this.deleteMe();
  }

  clearLog() {
    console.clear();
  }

  groupLog() {
    console.log('Outer level');
    console.group('group level 1');
    console.log('Level 1');
    console.group('group level 2');
    console.log('Level 2');
    console.log('still Level 2');
    console.group('group level 3');
    this.normalLog();
    console.groupEnd();
    console.groupEnd();
    console.log('back to Level 1');
    console.groupEnd();
  }

  assertLog() {
    console.assert(true, 'This won\'t be logged');
    console.assert(false, 'This will be logged');
  }

  countLog() {
    console.count('Count this!');
    console.count('Count this!');
    console.count('Count this!');
    console.count('Count this!');
  }

  breakpoints() {
    console.log('before breakpoint');
    debugger;
    console.log('after breakpoint');
  }

  onHover() {
    this.hoverClass = 'hovered';
    setTimeout(() => {
      this.hoverClass = null;
    }, 100);
  }

  conditionalBreakpoint(condition) {
    console.log(condition);
  }
}
